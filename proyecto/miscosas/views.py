from django.shortcuts import render
from datetime import datetime

# Create your views here.

#accedemos al archivo de datos para obtener las imagenes de votacion

path_foto_votar = "/static/mismiscosas/"
archivo_like={1:  ['like_sel.jpg', 'dis.jpg'],
            0:  ['like.jpg', 'dis.jpg'],
            -1:  ['like.jpg', 'dis_sel.jpg'],
}

#devolvemos error 404

def 404(request, url, context):
    resp = render(request, url, context)
    resp.status_code = 404
    return resp

def info(request):
    return render(request, 'miscosas/info.html', {'nav_info': "active", 'recurso_us': "/informacion"})

def procesar_users(request, lista):
    doc = request.GET['format']
    if doc == "xml":
        return HttpResponse(XML_create().xml_users(lista)
                            , content_type="text/xml")
    elif doc == "json":
        return HttpResponse(JSON_create().json_users(lista)
                            , content_type="application/json")
    else:
        context = {'error': _("Este documento no es compatible"), 'recurso_us': '/usuarios'}
        return devolver_404(request, 'miscosas/pag_error.html', context)

def users(request):
    lista = PagUsuario.objects.all()
    if 'format' in request.GET.keys():
        return procesar_users(request, lista)
    context = {'lista': lista, 'recurso_us': "/usuarios", 'nav_users': 'active'}
    return render(request, 'miscosas/users.html', context)

def procesar_alims(request, lista):
    doc = request.GET['format']
    if doc == "xml":
        return HttpResponse(XML_create().xml_alims(lista)
                            , content_type="text/xml")
    elif doc == "json":
        return HttpResponse(JSON_create().json_alims(lista)
                            , content_type="application/json")
    else:
        context = {'error': _("No se soporta ese tipo de documento"), 'recurso_us': '/alimentadores'}
        return 404(request, 'miscosas/error.html', context)

def alimentadores(request):
    lista = Alimentador.objects.all()
    if 'format' in request.GET.keys():
        return procesar_docs_alims(request, lista)
    context = {'lista': lista, 'recurso_us': "/alimentadores", 'nav_alims': 'active'}
    return render(request, 'miscosas/alimentadores.html', context)

#Devuelve el nombre de usuario o una cookie en caso de no estarlo
def nombre_persona(user):
    if user.is_authenticated:
        return user
    else:
        return ""

#almacena el usuario al que le ha dado a elegir al alimentador
def guardar_us_enalim(user, id):
    alim = Alimentador.objects.get(id=id)
    if nombre_persona(user) != "":
        alim.usuario.add(nombre_persona(user))

def leer_xml(tipo, nombre):
    if tipo == "yt":
        id = YTChannel(nombre).id_canal()
    elif tipo == "reddit":
        id = SubReddit(nombre).id_reddit()
    elif tipo == "fm":
        id = FMArtista(nombre).id_artista()
    return id




